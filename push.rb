require 'houston' 
 
      # Environment variables are automatically read, or can be       
      # conveniently use `Houston::Client.development` or `
      APN = Houston::Client.development 
      APN.certificate = File.read("/Users/godfrey/Documents/apple_push_notification.pem") 
 
      # An example of the token sent back when a device registers 
      token = "B5873FD05C7AD13F32DECCCF9086442B00680B88199AAAD51DE96D98CD9FD1EC"
 
      # Create a notification that alerts a 
      notification = Houston::Notification.new(device: token) 
      notification.alert = "Hello, World!" 
 
      # Notifications can also change the badge count, have a 
      notification.badge = 57 
      notification.sound = "sosumi.aiff" 
      notification.category = "INVITE_CATEGORY" 
      notification.content_available = true 
      notification.mutable_content = true 
      notification.custom_data = { foo: "bar" } 
 
      # And... sent! That's all it takes. 
      APN.push(notification) 
